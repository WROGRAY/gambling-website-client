import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      windowWidth: window.innerWidth,
      token: localStorage.getItem('token') ? localStorage.getItem('token') : '',
      coins: 0,
      username: localStorage.getItem('username') ? localStorage.getItem('username') : '',
      avatar: localStorage.getItem('avatar') ? localStorage.getItem('avatar') : '',
      steamid: localStorage.getItem('steamid') ? localStorage.getItem('steamid') : '',
      
      loadingTrade: '',
  },
  mutations: {
      setWidth(state, payload) {
          state.windowWidth = payload;
      },
      setToken(state, payload) {
          localStorage.setItem('token', payload);
          state.token = payload;
      },
      setCoins(state, payload) {
        state.coins = payload;
      },
      setUsername(state, payload) {
        localStorage.setItem('username', payload);
        state.username = payload;
      },
      setAvatar(state, payload) {
        localStorage.setItem('avatar', payload),
        state.avatar = payload;
      },
      setSteamID(state, payload) {
        localStorage.setItem('steamid', payload),
        state.steamid = payload;
      },
      setLoadingTrade(state, payload) {
        state.loadingTrade = payload;
      }
  },
  actions: {
  },
  modules: {
  },
  getters: {
    isAuthenticated: state => {
        return state.token.trim().length > 0;
    }
  }
  
})
